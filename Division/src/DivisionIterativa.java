import java.util.Scanner;

public class DivisionIterativa {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner(System.in);
		System.out.println("Hallar la division de un numero (MODO ITERATIVO)");
		System.out.println("\nDigite el numerador: ");
		int numerador = lector.nextInt();
		System.out.println("Digite el denominador: ");
		int denominador = lector.nextInt();

		System.out.println("El resultado es: " + dividir(numerador, denominador));
	}
	
	public static int dividir(int a, int b) {
		
		int resultado = 0;
		
		while(a>=b) {
			
			resultado += 1;
			a -= b;
		}
		
		return resultado;
	}

}
