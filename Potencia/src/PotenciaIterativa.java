import java.util.Scanner;

public class PotenciaIterativa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner lector = new Scanner(System.in);
		System.out.println("Hallar la potencia de un numero (MODO ITERATIVO)");
		System.out.println("\nDigite la base: ");
		int base = lector.nextInt();
		System.out.println("Digite la potencia: ");
		int potencia = lector.nextInt();
		
		System.out.println("El resultado es: "+potencia(base, potencia));
		
	}
	
	public static int potencia(int n, int m) {
		
		int resultado=1;
		
		if(m == 0)
			return 1;
		
		for(int i=0; i<m; i++) {
			
			resultado *= n;
		}
		return resultado;
	}

}
